/******************************************************************************
 * Laborator 01 - Zaklady pocitacove grafiky - IZG
 * ihulik@fit.vutbr.cz
 *
 * $Id: $
 * 
 * Popis: Hlavicky funkci pro funkce studentu
 *
 * Opravy a modifikace:
 * -
 */

#include "student.h"
#include "globals.h"

#include <time.h>

/******************************************************************************
 ******************************************************************************
 Funkce vraci pixel z pozice x, y. Je nutne hlidat frame_bufferu, pokud 
 je dana souradnice mimo hranice, funkce vraci barvu (0, 0, 0).
 Ukol za 0.5 bodu */
S_RGBA getPixel(int x, int y)
{
	if ((0 <= x) && (x < width) && (0 <= y) && (y < height))
		return frame_buffer[y * height + x];
	return makeBlackColor();
}

/******************************************************************************
 ******************************************************************************
 Funkce vlozi pixel na pozici x, y. Je nutne hlidat frame_bufferu, pokud 
 je dana souradnice mimo hranice, funkce neprovadi zadnou zmenu.
 Ukol za 0.5 bodu */
void putPixel(int x, int y, S_RGBA color)
{
	if ((0 <= x) && (x < width) && (0 <= y) && (y < height))
		frame_buffer[y * height + x] = color;
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na odstiny sedi. Vyuziva funkce GetPixel a PutPixel.
 Ukol za 1 bod. */
void grayScale()
{
	S_RGBA color;
	int x, y;
	unsigned char intensity;

	for (x = 0; x < width; x++)
		for (y = 0; y < height; y++)
		{
			color = getPixel(x, y);
			intensity = ROUND(0.299 * color.red + 0.587 * color.green + 0.114 * color.blue);
			color.red = intensity;
			color.green = intensity;
			color.blue = intensity;
			putPixel(x, y, color);
		}
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci algoritmu distribuce chyby.
 Ukol za 1 bod */
void distributeError(int x, int y, int error)
{
	S_RGBA color;
	unsigned char intensity;

	color = getPixel(x, y);
	intensity = ROUND(color.red + error);
	if (intensity < 0)
		intensity = 0;
	if (intensity > 255)
		intensity = 255;
	color.red = intensity;
	color.green = intensity;
	color.blue = intensity;
	putPixel(x, y, color);
}


void errorDistribution()
{
	S_RGBA color;
	int x, y, error;
	unsigned char intensity;

	grayScale();
	for (x = 0; x < width; x++)
		for (y = 0; y < height; y++)
		{
			color = getPixel(x, y);
			intensity = color.red;
			if (intensity <= 127)
				color = COLOR_BLACK;
			else
				color = COLOR_WHITE;
			error = intensity - color.red;
			putPixel(x, y, color);
			distributeError(x + 1, y, error * 3 / 8.0);
			distributeError(x, y + 1, error * 3 / 8.0 );
			distributeError(x + 1, y + 1, error / 4.0);
		}

}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci metody prahovani.
 Demonstracni funkce */
void thresholding(int Threshold)
{
	int y, x;

	/* Prevedeme obrazek na grayscale */
	grayScale();

	/* Projdeme vsechny pixely obrazku */
	for (y = 0; y < height; ++y)
		for (x = 0; x < width; ++x)
		{
			/* Nacteme soucasnou barvu */
			S_RGBA color = getPixel(x, y);

			/* Porovname hodnotu cervene barevne slozky s prahem.
			   Muzeme vyuzit jakoukoli slozku (R, G, B), protoze
			   obrazek je sedotonovy, takze R=G=B */
			if (color.red > Threshold)
				putPixel(x, y, COLOR_WHITE);
			else
				putPixel(x, y, COLOR_BLACK);
		}
}

/******************************************************************************
 ******************************************************************************
 Funkce prevadi obrazek na cernobily pomoci nahodneho rozptyleni. 
 Vyuziva funkce GetPixel, PutPixel a GrayScale.
 Demonstracni funkce. */
void randomDithering()
{
	int y, x;

	/* Prevedeme obrazek na grayscale */
	grayScale();

	/* Inicializace generatoru pseudonahodnych cisel */
	srand((unsigned int)time(NULL));

	/* Projdeme vsechny pixely obrazku */
	for (y = 0; y < height; ++y)
		for (x = 0; x < width; ++x)
		{
			/* Nacteme soucasnou barvu */
			S_RGBA color = getPixel(x, y);
			
			/* Porovname hodnotu cervene barevne slozky s nahodnym prahem.
			   Muzeme vyuzit jakoukoli slozku (R, G, B), protoze
			   obrazek je sedotonovy, takze R=G=B */
			if (color.red > rand()%255)
			{
				putPixel(x, y, COLOR_WHITE);
			}
			else
				putPixel(x, y, COLOR_BLACK);
		}
}
/*****************************************************************************/
/*****************************************************************************/